#include<stdio.h>

int main () {
    int N, i, j, s=0, p=1, min_d=10000000, min_s=10000000;
    printf("Numarul de linii=");
    scanf("%d", &N);
    printf("Numarul de coloane=");
    scanf("%d", &N);
    int a[N][N];
    for (i=0; i < N; i++)
        for (j=0; j < N; j++) {
                printf("a[%d][%d]=", i, j);
                scanf("%d", &a[i][j]);
        }
    for (i=0; i < N; i++) {
        for (j=0; j < N; j++) {
                if (i == j)
                    s=s+a[i][j];
                if (j == N-1-i)
                    p=p*a[i][j];
        }
    }
    for (i=0; i < N; i++) {
        for (j=0; j < N; j++) {
            if ((i < j) && (a[i][j] < min_d))
                min_d=a[i][j]; 
        }
    }
    for (i=0; i < N; i++) { 
        for (j=0; j < N; j++) {
            if ((i > j) && (a[i][j] < min_s))
                min_s=a[i][j];
        }
    }
    printf("Suma elementelor de pe diagonala principala este=%d\nProdusul elementelor de pe diagonala secundara este=%d\nMinimul elementelor aflate deasupra diagonalei principale este=%d\nMinimul elementelor aflate sub diagonala principala este=%d\n", s, p, min_d, min_s);
    return 0;
}

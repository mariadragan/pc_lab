#include<stdio.h>
#define N 100
#define M 100

int main () {
    int a[N][M], n, m, i, j, s=0;
    printf("Numarul de linii=");
    scanf("%d", &n);
    printf("Numarul de coloane=");
    scanf("%d", &m);
    for (i=0; i < n; i++)
        for (j=0; j < m; j++) {
                printf("a[%d][%d]=", i, j);
                scanf("%d", &a[i][j]);
        }
    for (i=0; i < m; i++) {
        s=s+a[0][i];
        s=s+a[n-1][i];
    }
    for (i=1; i < n-1; i++) {
        s=s+a[i][0];
        s=s+a[i][n-1];
    }
    printf("Suma elementelor de pe marginea matricei este=%d\n", s);
    return 0;
}

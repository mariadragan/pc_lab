#include<stdio.h>

int main () {
    int M, N, i, j, s=0;
    printf("Numarul de linii=");
    scanf("%d", &N);
    printf("Numarul de coloane=");
    scanf("%d", &M);
    int a[N][M];
    for (i=0; i < N; i++)
        for (j=0; j < M; j++) {
                printf("a[%d][%d]=", i, j);
                scanf("%d", &a[i][j]);
        }
    for (i=0; i < M; i++) {
        s=s+a[0][i];
        s=s+a[N-1][i];
    }
    for (i=1; i < N-1; i++) {
        s=s+a[i][0];
        s=s+a[i][N-1];
    }
    printf("Suma elementelor de pe marginea matricei este=%d\n", s);
    return 0;
}
